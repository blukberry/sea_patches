#!/bin/bash

cd ../

cd frameworks/av
git am --signoff < ~/ci/sea_patches/frameworks/av/0001-APM-Optionally-force-load-audio-policy-for-system-si.patch
git am --signoff < ~/ci/sea_patches/frameworks/av/0002-APM-Remove-A2DP-audio-ports-from-the-primary-HAL.patch
cd ../

cd base
git am --signoff < ~/ci/sea_patches/frameworks/base/0001-HACK-telephony-Conditionally-force-enable-LTE_CA.patch
cd ../../

cd packages/modules/Bluetooth
git am --signoff < ~/ci/sea_patches/packages/modules/Bluetooth/0001-audio_hal_interface-Optionally-use-sysbta-HAL.patch
cd ../../

cd apps/Settings
git am --signoff < ~/ci/sea_patches/packages/apps/Settings/0001-Settings-Add-a-toggle-to-force-LTE_CA.patch
cd ../../../
